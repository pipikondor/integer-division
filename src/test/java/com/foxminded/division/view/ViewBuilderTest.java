package com.foxminded.division.view;

import com.foxminded.division.model.DivisionData;
import com.foxminded.division.model.DivisionStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ViewBuilderTest {

    private ViewBuilder viewBuilder;
    private StringBuilder divisionView;

    @BeforeEach
    public void createViewBuilder() {
        viewBuilder = new ViewBuilder();
        divisionView = new StringBuilder();
    }

    @Test
    void buildView_shouldReturnViewWhichAddedOneSpacePerStep_whenDivisionIsConsistent() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(7, 4, 3, 1));
        steps.add(new DivisionStep(38, 36, 2, 2));
        steps.add(new DivisionStep(29, 28, 1, 3));
        DivisionData divisionData = new DivisionData(steps, 789, 4);
        divisionView.append("_789|4").append(System.lineSeparator());
        divisionView.append(" 4  |---").append(System.lineSeparator());
        divisionView.append(" -  |197").append(System.lineSeparator());
        divisionView.append("_38").append(System.lineSeparator());
        divisionView.append(" 36").append(System.lineSeparator());
        divisionView.append(" --").append(System.lineSeparator());
        divisionView.append(" _29").append(System.lineSeparator());
        divisionView.append("  28").append(System.lineSeparator());
        divisionView.append("  --").append(System.lineSeparator());
        divisionView.append("   1");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }

    @Test
    void buildView_shouldReturnViewWhichAddedTwoSpacePerStep_whenDivisionHasTwoNewDigitPerStep() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 12, 0, 2));
        steps.add(new DivisionStep(18, 18, 0, 4));
        DivisionData divisionData = new DivisionData(steps, 1218, 6);
        divisionView.append("_1218|6").append(System.lineSeparator());
        divisionView.append(" 12  |---").append(System.lineSeparator());
        divisionView.append(" --  |203").append(System.lineSeparator());
        divisionView.append("  _18").append(System.lineSeparator());
        divisionView.append("   18").append(System.lineSeparator());
        divisionView.append("   --").append(System.lineSeparator());
        divisionView.append("    0");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }

    @Test
    void buildView_shouldReturnOneStepView_whenDivisorLessThanDivider() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 0, 12, 2));
        DivisionData divisionData = new DivisionData(steps, 12, 456);
        divisionView.append("_12|456").append(System.lineSeparator());
        divisionView.append("  0|-").append(System.lineSeparator());
        divisionView.append(" --|0").append(System.lineSeparator());
        divisionView.append(" 12");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }

    @Test
    void buildView_shouldReturnOneStepViewWithMinusOffset_whenDivisorLessThanZero() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 10, 2, 3));
        DivisionData divisionData = new DivisionData(steps, -12, 5);
        divisionView.append("_-12|5").append(System.lineSeparator());
        divisionView.append("  10|--").append(System.lineSeparator());
        divisionView.append("  --|-2").append(System.lineSeparator());
        divisionView.append("  -2");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }

    @Test
    void buildView_shouldReturnViewWithPositiveReminder_whenDividerLessThanZero() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 10, 2, 2));
        DivisionData divisionData = new DivisionData(steps, 12, -5);
        divisionView.append("_12|-5").append(System.lineSeparator());
        divisionView.append(" 10|--").append(System.lineSeparator());
        divisionView.append(" --|-2").append(System.lineSeparator());
        divisionView.append("  2");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }

    @Test
    void buildView_shouldReturnViewWithNegativeReminderAndMinusOffset_whenDivisorLessThanZero() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 10, 2, 3));
        DivisionData divisionData = new DivisionData(steps, -12, -5);
        divisionView.append("_-12|-5").append(System.lineSeparator());
        divisionView.append("  10|-").append(System.lineSeparator());
        divisionView.append("  --|2").append(System.lineSeparator());
        divisionView.append("  -2");
        Assertions.assertEquals(divisionView.toString(), viewBuilder.buildView(divisionData));
    }
}
