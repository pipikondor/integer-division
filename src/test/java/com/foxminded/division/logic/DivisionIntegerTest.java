package com.foxminded.division.logic;

import com.foxminded.division.model.DivisionData;
import com.foxminded.division.model.DivisionStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class DivisionIntegerTest {

    private DivisionInteger divisionInteger;

    @BeforeEach
    public void createDivisionIntegerObject() {
        divisionInteger = new DivisionInteger();
    }

    @Test
    void divide_shouldThrowArithmeticException_whenDividerIsZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> divisionInteger.divide(20, 0));
    }

    @Test
    void divide_shouldReturnOneStep_whenDivisorGreaterThanDivider() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(7, 4, 3, 1));
        Assertions.assertEquals(new DivisionData(steps, 7, 4), divisionInteger.divide(7, 4));
    }

    @Test
    void divide_shouldAddDigitToNumberOfStep_whenDivisorLessThanDivider() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 0, 12, 2));
        Assertions.assertEquals(new DivisionData(steps, 12, 40), divisionInteger.divide(12, 40));
    }

    @Test
    void divide_shouldReturnStepWithoutMinus_whenDivisorLessZero() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(7, 4, 3, 2));
        Assertions.assertEquals(new DivisionData(steps, -7, 4), divisionInteger.divide(-7, 4));
    }

    @Test
    void divide_shouldAddDigitToNumber_whenDividerLessZero() {
        List<DivisionStep> steps = new ArrayList<>();
        steps.add(new DivisionStep(12, 10, 2, 2));
        Assertions.assertEquals(new DivisionData(steps, 12, -5), divisionInteger.divide(12, -5));
    }
}
