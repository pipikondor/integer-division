package com.foxminded.division.logic;

import com.foxminded.division.model.DivisionData;
import com.foxminded.division.model.DivisionStep;
import com.foxminded.division.model.StartStep;

import java.util.ArrayList;
import java.util.List;

public class DivisionInteger {

    public DivisionData divide(int divisor, int divider) {
        if (divider == 0) {
            throw new ArithmeticException();
        }
        List<DivisionStep> steps = getDivisionSteps(divisor, divider);
        return new DivisionData(steps, divisor, divider);
    }

    private List<DivisionStep> getDivisionSteps(int divisor, int divider) {
        String divisorAsString = Integer.toString(divisor);
        List<DivisionStep> divisionSteps = new ArrayList<>();
        int cursor = 0;
        if (divisor < 0) {
            cursor++;
        }
        int numberForStep = Character.getNumericValue(divisorAsString.charAt(cursor));
        cursor++;
        do {
            StartStep step = new StartStep(numberForStep, cursor);
            DivisionStep divisionStep = buildStep(step, Math.abs(divider), divisorAsString);
            divisionSteps.add(divisionStep);
            numberForStep = divisionStep.getReminder();
            cursor = divisionStep.getCursor();
        } while (cursor < divisorAsString.length());
        return divisionSteps;
    }

    private DivisionStep buildStep(StartStep currentStep, int divider, String divisorAsString) {
        int numberForStep = currentStep.getNumber();
        int cursor = currentStep.getCursor();
        while (numberForStep < divider && cursor < divisorAsString.length()) {
            numberForStep = addDigitToNumber(numberForStep, cursor, divisorAsString);
            cursor++;
        }
        int subtractor = getSubtractor(numberForStep, divider);
        int reminder = numberForStep - subtractor;
        return new DivisionStep(numberForStep, subtractor, reminder, cursor);
    }

    private int getSubtractor(int numberForStep, int divider) {
        return divider * (numberForStep / divider);
    }

    private int addDigitToNumber(int numberForStep, int cursor, String divisorAsString) {
        String numberForStepAsString = String.valueOf(numberForStep) + divisorAsString.charAt(cursor);
        return Integer.parseInt(numberForStepAsString);
    }
}
