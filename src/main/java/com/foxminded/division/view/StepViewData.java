package com.foxminded.division.view;

import com.foxminded.division.model.DivisionStep;

public class StepViewData {

    private final int numberOffsetLength;
    private final int subtractorOffsetLength;
    private final int lineLength;

    public StepViewData(DivisionStep step) {
        lineLength = getNumberLength(step.getNumber());
        numberOffsetLength = step.getCursor() - lineLength;
        subtractorOffsetLength = step.getCursor() - getNumberLength(step.getSubtractor()) + 1;
    }

    private int getNumberLength(int number) {
        return String.valueOf(number).length();
    }

    public int getNumberOffsetLength() {
        return numberOffsetLength;
    }

    public int getSubtractorOffsetLength() {
        return subtractorOffsetLength;
    }

    public int getLineLength() {
        return lineLength;
    }
}
