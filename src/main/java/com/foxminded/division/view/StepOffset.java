package com.foxminded.division.view;

public class StepOffset {

    private final String numberOffset;
    private final String subtractorOffset;
    private final String lineOffset;
    private final String line;

    public StepOffset(String numberOffset, String subtractorOffset, String line) {
        this.numberOffset = numberOffset;
        this.subtractorOffset = subtractorOffset;
        this.lineOffset = numberOffset + " ";
        this.line = line;
    }

    public String getNumberOffset() {
        return numberOffset;
    }

    public String getSubtractorOffset() {
        return subtractorOffset;
    }

    public String getLineOffset() {
        return lineOffset;
    }

    public String getLine() {
        return line;
    }
}
