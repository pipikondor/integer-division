package com.foxminded.division.view;

import com.foxminded.division.model.DivisionData;
import com.foxminded.division.model.DivisionStep;

import java.util.List;

public class ViewBuilder {

    private static final char MINUS_SYMBOL = '_';
    private static final char DELIMITER_SYMBOL = '|';
    private static final char SPACE_SYMBOL = ' ';
    private static final char LINE_SYMBOL = '-';

    public String buildView(DivisionData divisionData) {
        int i;
        boolean negativeReminder = false;
        List<DivisionStep> steps = divisionData.getDivisionSteps();
        StringBuilder viewOfDivision = new StringBuilder();
        viewOfDivision.append(buildFirstStep(steps.get(0), divisionData.getDivisor(), divisionData.getDivider()));
        viewOfDivision.append(System.lineSeparator());
        for (i = 1; i < steps.size(); i++) {
            viewOfDivision.append(buildStep(steps.get(i)));
            viewOfDivision.append(System.lineSeparator());
        }
        if (divisionData.getDivisor() < 0) {
            negativeReminder = true;
        }
        viewOfDivision.append(buildReminder(steps.get(i - 1), negativeReminder));
        return viewOfDivision.toString();
    }

    private String buildFirstStep(DivisionStep firstStep, int divisor, int divider) {
        int resultOfDivision = divisor / divider;
        int offsetLength = String.valueOf(divisor).length() - firstStep.getCursor();
        int dividedLineLength = String.valueOf(resultOfDivision).length();
        String[] stepRows = buildStep(firstStep).split(System.lineSeparator());
        stepRows[0] = String.format("%s%d%s%d", MINUS_SYMBOL, divisor, DELIMITER_SYMBOL, divider);
        String offset = buildStringByChar(offsetLength, SPACE_SYMBOL);
        String dividedLine = buildStringByChar(dividedLineLength, LINE_SYMBOL);
        stepRows[1] = String.format("%s%s%s%s", stepRows[1], offset, DELIMITER_SYMBOL, dividedLine);
        stepRows[2] = String.format("%s%s%s%d", stepRows[2], offset, DELIMITER_SYMBOL, resultOfDivision);
        return String.join(System.lineSeparator(), stepRows);
    }

    private String buildStep(DivisionStep step) {
        StringBuilder stepView = new StringBuilder();
        StepOffset stepOffset = buildOffsets(step);
        stepView.append(stepOffset.getNumberOffset());
        stepView.append(MINUS_SYMBOL);
        stepView.append(step.getNumber());
        stepView.append(System.lineSeparator());
        stepView.append(stepOffset.getSubtractorOffset());
        stepView.append(step.getSubtractor());
        stepView.append(System.lineSeparator());
        stepView.append(stepOffset.getLineOffset());
        stepView.append(stepOffset.getLine());
        return stepView.toString();
    }

    private String buildReminder(DivisionStep step, boolean negativeReminder) {
        int reminderLength = String.valueOf(step.getReminder()).length();
        int reminderOffset = step.getCursor() - reminderLength;
        char symbolOfReminder = SPACE_SYMBOL;
        String offset = buildStringByChar(reminderOffset, SPACE_SYMBOL);
        if (negativeReminder) {
            symbolOfReminder = LINE_SYMBOL;
        }
        return offset + symbolOfReminder + step.getReminder();
    }

    private StepOffset buildOffsets(DivisionStep step) {
        StepViewData stepViewData = new StepViewData(step);
        String numberOffset = buildStringByChar(stepViewData.getNumberOffsetLength(), SPACE_SYMBOL);
        String subtractorOffset = buildStringByChar(stepViewData.getSubtractorOffsetLength(), SPACE_SYMBOL);
        String line = buildStringByChar(stepViewData.getLineLength(), LINE_SYMBOL);
        return new StepOffset(numberOffset, subtractorOffset, line);
    }

    private String buildStringByChar(int offsetLength, char offsetChar) {
        StringBuilder offsetString = new StringBuilder();
        for (int i = 0; i < offsetLength; i++) {
            offsetString.append(offsetChar);
        }
        return offsetString.toString();
    }
}
