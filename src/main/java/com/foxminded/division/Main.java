package com.foxminded.division;

import com.foxminded.division.logic.DivisionInteger;
import com.foxminded.division.view.ViewBuilder;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number = 0;
        int divider = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number: ");
        if (scanner.hasNext()) {
            number = scanner.nextInt();
        }
        System.out.println("Enter the divider: ");
        if (scanner.hasNext()) {
            divider = scanner.nextInt();
        }
        DivisionInteger divisionInteger = new DivisionInteger();
        ViewBuilder view = new ViewBuilder();
        System.out.println(view.buildView(divisionInteger.divide(number, divider)));
    }
}
