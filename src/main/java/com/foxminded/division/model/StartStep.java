package com.foxminded.division.model;

public class StartStep {

    private final int number;
    private final int cursor;

    public StartStep(int number, int cursor) {
        this.number = number;
        this.cursor = cursor;
    }

    public int getNumber() {
        return number;
    }

    public int getCursor() {
        return cursor;
    }
}
