package com.foxminded.division.model;

import java.util.Objects;

public class DivisionStep {

    private final int number;
    private final int subtractor;
    private final int reminder;
    private final int cursor;

    public DivisionStep(int number, int subtractor, int reminder, int cursor) {
        this.number = number;
        this.subtractor = subtractor;
        this.reminder = reminder;
        this.cursor = cursor;
    }

    public int getReminder() {
        return reminder;
    }

    public int getNumber() {
        return number;
    }

    public int getSubtractor() {
        return subtractor;
    }

    public int getCursor() {
        return cursor;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DivisionStep that = (DivisionStep) object;
        return number == that.number && subtractor == that.subtractor && reminder == that.reminder && cursor == that.cursor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, subtractor, reminder, cursor);
    }
}
