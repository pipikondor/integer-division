package com.foxminded.division.model;

import java.util.List;
import java.util.Objects;

public class DivisionData {

    private final List<DivisionStep> divisionSteps;
    private final int divisor;
    private final int divider;

    public DivisionData(List<DivisionStep> divisionSteps, int divisor, int divider) {
        this.divisionSteps = divisionSteps;
        this.divisor = divisor;
        this.divider = divider;
    }

    public List<DivisionStep> getDivisionSteps() {
        return divisionSteps;
    }

    public int getDivisor() {
        return divisor;
    }

    public int getDivider() {
        return divider;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        DivisionData that = (DivisionData) object;
        return divisor == that.divisor && divider == that.divider && Objects.equals(divisionSteps, that.divisionSteps);
    }

    @Override
    public int hashCode() {
        return Objects.hash(divisionSteps, divisor, divider);
    }
}
